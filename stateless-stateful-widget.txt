what is State ?

In easy way that  stuff where change can come .That change is called state.

For eg : -- You are watching videos and you are sitting, what is your state?

ans)You are sitting.

If you stand then you state will change ie, you are standing.

If you start walking ,you state will change ie you are walking.

If you sleep , then again you state will change . You are sleeping.

So if any changes comes in any stuffs , so basically we are stating , it'state has changed (usko state me badlao hue he).


IMP IMP IMP IMP IMP

If we talk about app how state matters here ?


If there are no changes in the screen once that screen renders i.e once that screen starts to show in mobile , there will be no changes in it after that .That means it is stateless.


Right now it is stateless, because we are not doing anything .When i click on login button , the whole screen changes.

Inside screen , no change is occuring(screen ke andar kuch change nhi hota but whole screen is changing.)

That's why it's working on stateless.


But assume when i click on login , instead of going to next screen it changes the welcome text and do something else .

some change should occur on screen.

For that stuff to get done we have to make it  i.e top most class stateful.

That is if there is any change in the screen ,  we have to make that component or screen stateful.

Imp Imp Imp Imp

Now you can say  there is some chnages in username  when we type in it . So little bit changes is occuring in the screen .

So why is it not Stateful? Why is it stateless?

It's not like that the ---

The username component for which we are using --- TextFormField.If you open it and go inside --

see it extends FormField .If you open FormField , what is it extending?

It is extending Statefulwidget .

Every component can maintain it's own state .If we do that our app performance improves.

So if change our screen again and again ,But only text has to change .But we are rendering the screen again and again , performance issues will be there.

So when we have to change state , we will use stateful widget.

In many places you will not see stateful widget directly  , but it can be that in background in any place state is changing.

***********************************************************************************************

==>Right now we have only StatelessWidget in which only class gets created 

==>In StatefulWidget 2 class gets created.
*********************************************************************************
I want the user name to start getting added after welcome as we type

To do that  select that class in Login page and see that bulb there , click on it. It has a shortcut called Convert to StatefulWidget or Convert class to mixin -----

If we click on it, there will be a little bit change ,

==>Now LoginPage class is extending StatefulWidget instead of StatelessWidget ---

                class LoginPage extends StatefulWidget {

It is using StatefulWidget.

==>But along with it, it  has created another class ----

        class _LoginPageState extends State<LoginPage> {

        }


which is State class whose name is __LoginPageState and it is extending State class .That means through it we can acess state.

If we go State class by clicking on it ---

This State clasx Extends StatefulWidget.

So idea is we get State class .with this state class , we can  get an opportunity to refresh this whole screen.

So basicallly 

IMP IMP IMP IMP

==>In StatelessWidget only 1 class gets involved 

==>In StatefulWidget 2 class gets involved.

Why there is underscore before the State class --> _LoginPageState


It's not neccessary to put underscore "_", you can remove it.But the advantage of using "_",

To make any stuff private inside  dart we will use "_"(underscore)

Assume there is a Variable  whose name is employeeName. Now this employeeName is public.If i have to make it public , we put "_" before the Variable , it will become private .

private means this stuff , assume this class is  private other than this file no one can acess it. 

Like that if any field is private inside any class ,other than this class no one can acess it. i.e no one can use it by taking it outside .


**************************************Use of state**********************

==>Assume we have a String, By default it is empty .

class _LoginPageState extends State<LoginPage> {
  String name="";


==>Now where we are using Welcome , I will put the Variable name after that like these  
Text("Welcome $name",

==>Since it is empty , so nothing is happening , If i start typing pawan, name is name right now and there is no error in the app.

==>Now I want as  i type I want  this name Variable to get changing according to the typed text ---

==>The TextFormField where we have Username if i can find any property with which we  change control or stuffs --

==>1)onFieldSubmitted

==>2)onEditingCompleted

==>3)onChanged --->onChanged means whenever there is some chnage in that Text field , then we will get  a callback .It will give us a String .This is callback in which we are getting a value--

            TextFormField(
                  decoration: InputDecoration(
                    hintText: "Enter User Name",
                    labelText: "Username",
                  ),
                  onChanged: (value) => {name = value},
                ),

And let's give that value to name .

==>Now if i type my name, then nothing is happening here but we had put name=value.So something should happen here .But there is no change 


==>The State class that we are extending  and this widget , to redraw it  i.e this whole page . to redraw it again redraw as in those widgets which have already got built,to rebuilt those again.

How will it happen?

When we will call BuildMethod again  because inside it the whole UI is written inside it.


To call BuildMethod again -------

Imp Imp Imp
==>If I say build here like this ----

        onChanged: (value)  {
                    name = value;
                    build(context);
                },

It will get called. But it is a wrong way.It will create such a situation for which your code will break.

Imp Imp Imp Imp
==>So we have a way to change state ----

For that we use a method called ------- setState()

If we try to use setState in statelessWidget , you will get error.It works for stateful widget only.

Imp Imp Imp
What setState does?

As soon as you call setState , it change your UI i.e it will call build() method again.

TextFormField(
                  decoration: InputDecoration(
                    hintText: "Enter User Name",
                    labelText: "Username",
                  ),
                  onChanged: (value)  {
                    name = value;
                    setState(() {
                      
                    });
                    },
                ),

Now see it is working.


**********************AnimatedContainer | Easy Animations****************************
For now we are using ElevatedButton .

Instead of ElevatedButton,

NEW WIDGET -->Container
Let's see a new Widget called Container -----

Container is a Box.

***********************PROPERTY OF CONATAINER****************************************
Now this button ,if you don't know if there are any widgets named ElevatedButton , So if you have to create a Widget like that ----

        HOW WILL YOU CREATE IT?

        ANS)YOU CAN TAKE Container WHICH MEANS A BOX.It does not contain anything now.
        NOW YOU HAVE TO MENTION IT'S height and width.
        So now it became a box , whose height is 50 and width is 100.
        We cannot see it yet, because it's color is white bydefault.
A)height

B)width

c)colors:

d)alignment:To center the text, we have a property called alignment . alignment takes Alignment and you can align it i.e where to place it.

e)decoration -- Container has another property called Decoration which takes  BoxDecoration widget.we have to decorate the Container.

Since you have already put the color in Container, in DecorationBox also we have color property .
There is no meaning in putting color in 2 places .So put the color in DecorationBox.

But if you try to put color in 2 places there will be error .color property cannot be in  2 places.

        decoration: BoxDecoration(
                                color: Color.fromRGBO(143, 189, 97, 43),
                            
        ),
Imp Imp Imp 
When you are using decoration , color property should be inside  decoration only .
****************Decoration Properties of CONATAINER**************************
f)borderRadius-Now we have to give a border radius so there is not sharp edges in the button .

borderRadius takes geometry , for that we have a widget --

            BorderRadius.circular

        decoration: BoxDecoration(
                        color: Color.fromRGBO(143, 189, 97, 43),
                        borderRadius: BorderRadius.circular(15)
                    ),

g)Now we can't click it. 

        WHY?
Container is a box , there is no property of click in box.In ElevatedButton there is property of Click.

*********************Clickable Widget | Designing Button with Container********************

If you have to make any widget Clickable.There are 2 ways -----

a)GestureDetector -->you can wrap it with GestureDetector to make it clickable.


b)InkWell --> You can wrap it with InkWell to make it clickable.


what's the difference beteen GestureDetector and InkWell?

==>GestureDetector does not give you any feedback ,feedback as in UI repel effect will not be seen . That means you will not feel that you are clicking it.Bas tab hoga or wo usko perform kar dega .

==>InkWell -- What is the advantage of InkWell?
You will see the clicking effect. you will splash color when you click it i.e repel color.

Now in InkWell you will get all the properties like 



a)onTab --When you click on the button.onTab is similar to onPressed.


****************************************************AnimatedContainer*******************************************************

NewWidget

AnimatedContainer is a stuff , a widget which is like a Container but you will get Animations in it. 

Lets's make that Container ---AnimatedContainer .

ERROR 
Now as soon as you make that , it will give us an error , give duration in it atleast or a curve.

curve is optional.

a)duration -duration takes Duration and we will give 1s  so takes 1s and do some Animations in it.
    child: AnimatedContainer(
                    duration: Duration(seconds: 1),  

        When will this Animation occur?

    Now instead of navigating users to home_page right now we are doing it. Let's not do it for now , let's comment  it for now .



Lets's make a boolean here ---
        bool changedButton=false;
        bydefault it is false.
Now what I want ?
When I click on this Button , the changedButton Variable make it true .When we are making it true, whenever we are changing any state , we need to use --

                setState

                InkWell(
                  onTap: () {
                    // Navigator.pushNamed(context, AppRoutes.homeRoute);
                    setState(() {
                      changedButton = true;
                    });
                  },

***************Condition Expression in Flutter******************
   width: changedButton?100:200,

b)curve(optional)


To make a circular button -----

a)shape

b)rectangular

 shape: changedButton
            ? BoxShape.circle
            : BoxShape.rectangle,

Now if you want use to shape, you have to remove borderRadius ,there is a little contradiction between them.If you want to use both , you will face problem.

Now what i want  as soon as the Animations complete this login text insetad of that, we want an icon.

a)Icon
child: changedButton
    ? Icon(Icons.done,color: Colors.white)
    : Text(
        "Login",
        style: TextStyle(
        color: Color.fromARGB(255, 254, 252, 252),
        fontWeight: FontWeight.w700,
        fontSize: 20.0,
        ),
    )


Now as soon this Text changes , after  1 sec ,  I want to go to next page ----

I will do await here i,e wait 

How log i have to wait ?

We will use Future.delay , i.e we can make a delay of 1s 

 await Future.delayed(
                      Duration(seconds: 1)
    )


Now when you write await, you method should be async i.e asynchronous.


onTap: () async {
                    // Navigator.pushNamed(context, AppRoutes.homeRoute);
                    setState(() {
                      changedButton = true;
                    });
                    await Future.delayed(Duration(seconds: 1));
            },
Wait for 1s after that do what?
ans)Navigate to HomePage