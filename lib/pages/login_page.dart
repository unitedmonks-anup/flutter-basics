import "package:flutter/material.dart";
import 'package:flutter_catalog/utils/routes.dart';

class LoginPage extends StatefulWidget {
  // final _employeeName="Anup";
  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  String name = "";
  bool changedButton = false;
  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.white,
      child: SingleChildScrollView(
        child: Column(
          children: [
            Image.asset(
              "assets/images/login_bg.png",
              fit: BoxFit.cover,
              height: 300,
            ),
            SizedBox(height: 20.0),
            Text("Welcome , $name",
                style: TextStyle(fontSize: 28, fontWeight: FontWeight.w700)),
            SizedBox(
              height: 20.0,
              // child: Text("jssssjsj")
            ),
            Padding(
              padding:
                  const EdgeInsets.symmetric(vertical: 16.0, horizontal: 32.0),
              child: Column(children: [
                TextFormField(
                  decoration: InputDecoration(
                    hintText: "Enter User Name",
                    labelText: "Username",
                  ),
                  onChanged: (value) {
                    name = value;
                    setState(() {});
                  },
                ),
                TextFormField(
                    obscureText: true,
                    decoration: InputDecoration(
                      hintText: "Enter Your Pasword",
                      labelText: "Password",
                    )),
                SizedBox(height: 40.0),
                InkWell(
                  onTap: () async {
                    setState(() {
                      changedButton = true;
                    });
                    await Future.delayed(Duration(seconds: 1));

                    Navigator.pushNamed(context, AppRoutes.homeRoute);
                  },
                  child: AnimatedContainer(
                      duration: Duration(seconds: 1),
                      width: changedButton ? 50 : 150,
                      height: 50,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          color: Color.fromRGBO(143, 189, 97, 43),
                          // shape: changedButton
                          //     ? BoxShape.circle
                          //     : BoxShape.rectangle,
                          borderRadius:
                              BorderRadius.circular(changedButton ? 50 : 8)),
                      child: changedButton
                          ? Icon(Icons.done, color: Colors.white)
                          : Text(
                              "Login",
                              style: TextStyle(
                                color: Color.fromARGB(255, 254, 252, 252),
                                fontWeight: FontWeight.w700,
                                fontSize: 20.0,
                              ),
                            )),
                )
                // ElevatedButton(
                //     onPressed: () {
                //       Navigator.pushNamed(context, AppRoutes.homeRoute);
                //     },
                //     child: Text("Login"),
                //     style: TextButton.styleFrom(minimumSize: Size(150, 50)))
              ]),
            )
          ],
        ),
      ),
    );
  }
}
